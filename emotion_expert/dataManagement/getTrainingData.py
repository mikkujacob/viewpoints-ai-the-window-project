import os

def getTrainingData(within, category):
    trainingData = []
    for label in ["pos", "neg"]:
        for filename in os.listdir("%s/%s/%s" % (within, category, label)):
            trainingData.append((getGestureParameters("%s/%s/%s/%s" % (within, category, label, filename)), 1 if label == "pos" else 0))
    return trainingData
        
def getGestureParameters(filename):
    with open(filename) as file:
        return getGestureParametersFromLines(filter(lambda line : line, file))

def getGestureParametersFromLines(lines):
    parameters = dict()
    for line in lines:
        key, value = line.split(':')
        value = float(value)
        parameters[key] = value
    return parameters
            