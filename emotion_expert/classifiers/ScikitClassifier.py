import numpy as np
from classifiers.scorer import fMeasure
from math import isnan
from sklearn.cross_validation import ShuffleSplit, cross_val_score, LeaveOneOut
import random

class NotSupportedException:
    pass

class ScikitClassifier:
    def __init__(self):
        pass

    def _formatLibrary(self, library):
        featureTable = np.zeros((len(library), len(self._relevantParameters)), dtype = np.float64)
        target = np.zeros(len(library), dtype = np.bool)
        for i, (parameters, label) in enumerate(library):
            featureTable[i] = [parameters[relevant] for relevant in self._relevantParameters]
            target[i] = label
        return (featureTable, target)
    
    def cvOnLibrary(self, library):
        X, y = self._formatLibrary(library)
        yt = []
        yp = []
        for i in xrange(len(X)):
            print(i)
            Xtest, ytest = (X[[i]], y[[i]])
            rest = [j for j in xrange(len(X)) if j != i]
            Xtrain, ytrain = (X[rest], y[rest])
            classifier = self.clone()
            classifier.fit(Xtrain, ytrain)
            ypred = classifier.predict(Xtest)
            yt.append(ytest[0])
            yp.append(ypred[0])
        return fMeasure(np.array(yp), np.array(yt))
    
    def trainOnLibrary(self, library):
        self._classifier.fit(*self._formatLibrary(library))
        
    def classify(self, sample):
        featureVector = np.array([[sample[relevant] for relevant in self._relevantParameters]])
        output = self._classifier.predict(featureVector)
        return "neg" if 0 == output[0] else "pos"
    
    def autotuneIntOnLibrary(self, library, tuneRange, nTries = 1, loud = True):
        if len(tuneRange) != 1:
            return NotSupportedException
        tuneParam, (tuneRangeDn, tuneRangeUp) = tuneRange[0]
        params = self.get_params()
        results = []
        for tuneValue in xrange(tuneRangeDn, tuneRangeUp + 1):
            if loud:
                print(tuneValue)
            cvs = []
            for i in xrange(nTries):
                params[tuneParam] = tuneValue
                newTune = self.__class__(**params)
                cvs.append(newTune.cvOnLibrary(library))
            cvs = np.array(cvs)
            result = dict()
            for key in ['precision', 'recall', 'fMeasure']:
                vals = np.array([cv[key] for cv in cvs])
                result[key] = vals.mean()
            results.append((tuneValue, result))
        return max(results, key = lambda (tuneValue, result) : result['fMeasure'])
            
    def autotuneFloatOnLibrary(self, library, tuneRange, tolerance, loud = True):
        if len(tuneRange) != 1:
            return NotSupportedException
        params = self.get_params()
        tuneParam, (tuneRangeDn, tuneRangeUp) = tuneRange[0]
        intervalDn = tuneRangeDn
        intervalUp = tuneRangeUp
        def applyTuneValue(tuneValue):
            params[tuneParam] = tuneValue
            newTune = self.__class__(**params)
            return newTune.cvOnLibrary(library)['fMeasure']
        intervalValueUp = applyTuneValue(intervalUp)
        intervalValueDn = applyTuneValue(intervalDn)
        def tuneStep(iDn, iUp, valDn, valUp):
            if abs(valDn - valUp) < tolerance:
                return (valDn, iDn) if valDn >= valUp else (valUp, iUp)
            if loud:
                print("%.4f - %.4f" % (iDn, iUp))
            delta = iUp - iDn
            iThird = iDn + delta * (1 / 3.0)
            iTwoThirds = iDn + delta * (2 / 3.0)
            valThird = applyTuneValue(iThird)
            valTwoThirds = applyTuneValue(iTwoThirds)
            if valThird > valTwoThirds:
                return tuneStep(iDn, iTwoThirds, valDn, valTwoThirds)
            elif valThird < valTwoThirds:
                return tuneStep(iThird, iUp, valThird, valUp)
            else:
                return max(tuneStep(iDn, iThird, valDn, valThird), tuneStep(iTwoThirds, iUp, valTwoThirds, valUp))
        return tuneStep(intervalDn, intervalUp, intervalValueDn, intervalValueUp)
