from classifiers.ScikitClassifier import ScikitClassifier
import numpy as np
from sklearn.mixture.gmm import GMM
from sklearn.cross_validation import cross_val_score, LeaveOneOut,\
    LeaveOneLabelOut
from math import log

class ScikitMixtureOfGaussiansClassifier(ScikitClassifier):
    def __init__(self, relevantParameters, k):
        self._k = k
        self._positiveMixture = GMM(n_components = k, covariance_type = 'diag')
        self._negativeMixture = GMM(n_components = 1, covariance_type = 'diag')
        self._positiveLogPrior = 0
        self._negativeLogPrior = 0
        self._relevantParameters = relevantParameters
        
    def clone(self):
        return ScikitMixtureOfGaussiansClassifier(self._relevantParameters, self._k)
        
    def fit(self, X, y):
        Xpos = X[y]
        Xneg = X[np.invert(y)]
        self._positiveMixture.fit(Xpos)
        self._negativeMixture.fit(Xneg)
        #positivePrior = 0.01
        #negativePrior = 0.99
        positivePrior = float(np.sum(y)) / len(y)
        negativePrior = 1.0 - positivePrior
        self._positiveLogPrior = log(positivePrior)
        self._negativeLogPrior = log(negativePrior)
    
    def predict(self, X):
        positiveScores = self._positiveLogPrior + self._positiveMixture.score_samples(X)[0]
        negativeScores = self._negativeLogPrior + self._negativeMixture.score_samples(X)[0]
        return positiveScores > negativeScores
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "k" : self._k}