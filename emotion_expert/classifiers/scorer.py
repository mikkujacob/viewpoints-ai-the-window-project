import numpy as np
from math import isnan

def errorRateOf(classifier, on):
    errors = 0
    for parameters, label in on:
        if label != classifier.classify(parameters):
            errors += 1
    return float(errors) / len(on)

def precisionRecallFMeasureOf(classifier, on):
    truePositives = 0
    falsePositives = 0
    falseNegatives = 0
    for parameters, label in on:
        predictedLabel = classifier.classify(parameters)
        if label and predictedLabel:
            truePositives += 1 
        elif not label and predictedLabel:
            falsePositives += 1
        elif label and not predictedLabel:
            falseNegatives += 1
    precision = truePositives / float(truePositives + falsePositives)
    recall = truePositives / float(truePositives + falseNegatives)
    fMeasure = 2 * precision * recall / (precision + recall)
    return {"precision" : precision, "recall" : recall, "fMeasure" : fMeasure}

def fMeasure(yp, y):
    truePositives = np.sum(np.logical_and(yp, y))
    falsePositives = np.sum(np.logical_and(yp, np.logical_not(y)))
    falseNegatives = np.sum(np.logical_and(np.logical_not(yp), y))
    precision = truePositives / float(truePositives + falsePositives)
    recall = truePositives / float(truePositives + falseNegatives)
    fMeasure = 2* precision * recall / (precision + recall)
    fMeasure = fMeasure if not isnan(fMeasure) else 0
    return {"precision" : precision, "recall" : recall, "fMeasure" : fMeasure}