from collections import defaultdict
from math import sqrt, exp, log
import random
import sys

LOGMAX = log(sys.float_info.max) - 10.0

class LogisticRegressionClassifier: #online version of multiclass logistic regression
    def __init__(self, relevantParameters, nu, cutoff):
        self._relevantParameters = set(relevantParameters)
        self._nu = nu #gradient descent parameter
        self._cutoff = cutoff
        self._weights = defaultdict(float)
        random.seed()
        
    def trainOnExample(self, trainLabel, parameters):
        sigmaPositive = self._sigmaPositive(parameters)
        adjustment = (1 if trainLabel == "pos" else 0) - sigmaPositive
        self._weights[1] = self._weights[1] + self._nu * adjustment
        for paramkey, paramval in filter(lambda (k, v) : k in self._relevantParameters, parameters.items()):
            self._weights[paramkey] = self._weights[paramkey] + self._nu * adjustment * paramval
                
    def trainOnLibrary(self, library):
        library = list(library)
        dWeights = defaultdict(float)
        for parameters, label in library:
            sigmaPositive = self._sigmaPositive(parameters)
            adjustment = (1 if label == "pos" else 0) - sigmaPositive
            dWeights[1] = dWeights[1] + self._nu * adjustment
            for paramkey, paramval in filter(lambda (k, v) : k in self._relevantParameters, parameters.items()):
                dWeights[paramkey] = dWeights[paramkey] + self._nu * adjustment * paramval
        for paramkey, delta in dWeights.items():
            self._weights[paramkey] += dWeights[paramkey]
        self.degradeWeights()
                       
    def degradeWeights(self): #an attempt to counter overfitting by regularizing weights
        sqrnorm = 0
        for feature in self._weights.keys():
            sqrnorm += pow(self._weights[feature], 2)
        wnorm = sqrt(sqrnorm)
        if wnorm > self._cutoff:
            scale = sqrt(self._cutoff / wnorm)
            for feature in self._weights.keys():
                self._weights[feature] *= scale
                
    def _sigmaPositive(self, parameters):
        activationValue = self._weights[1] + sum(self._weights.get(paramkey, 0) * paramvalue 
                                                 for paramkey, paramvalue in parameters.items()
                                                 if paramkey in self._relevantParameters)
        if activationValue > LOGMAX:
            return 1.0
        elif activationValue < -LOGMAX:
            return 0.0
        else:
            expvalue = exp(activationValue)
            return expvalue / (expvalue + 1)
    
    def classify(self, parameters):
        sigmaPositive = self._sigmaPositive(parameters)
        return "pos" if sigmaPositive > 0.5 else "neg"