#Rules for initializing a table of opposing values to be used for selecting a new value for a transformed parameter

sp { initialize*state*body_symmetric
   (state <ss> ^type state)
-->
	(<ss> ^body_symmetric <s> <a> <none>)
	(<s> ^value SYMMETRIC ^opposite ASYMMETRIC)
	(<a> ^value ASYMMETRIC  ^opposite SYMMETRIC)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*facing
   (state <ss> ^type state)
-->
	(<ss> ^facing <fl> <sl> <c1> <c2> <sr> <fr> <none>)
	(<fl> ^value FAR_LEFT ^opposite FAR_RIGHT)
	(<sl> ^value SLIGHT_LEFT ^opposite SLIGHT_RIGHT)
	(<c1> ^value CENTER ^opposite FAR_LEFT)
	(<c2> ^value CENTER ^opposite FAR_RIGHT)
	(<sr> ^value SLIGHT_RIGHT ^opposite SLIGHT_LEFT)
	(<fr> ^value FAR_RIGHT ^opposite FAR_LEFT)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*height
   (state <ss> ^type state)
-->
	(<ss> ^height <s> <m1> <m2> <t> <none>)
	(<s> ^value SHORT ^opposite TALL)
	(<m1> ^value MEDIUM ^opposite TALL)
	(<m2> ^value MEDIUM ^opposite SHORT)
	(<t> ^value TALL ^opposite SHORT)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*quadrant
   (state <ss> ^type state)
-->
	(<ss> ^quadrant <tr> <tl> <br> <bl> <none>)
	(<tr> ^value TOP_RIGHT ^opposite BOTTOM_LEFT)
	(<tl> ^value TOP_LEFT ^opposite BOTTOM_RIGHT)
	(<br> ^value BOTTOM_RIGHT ^opposite TOP_LEFT)
	(<bl> ^value BOTTOM_LEFT ^opposite TOP_RIGHT)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*dist_center
   (state <ss> ^type state)
-->
	(<ss> ^dist_center <n> <m1> <m2> <f> <none>)
	(<n> ^value NEAR ^opposite FAR)
	(<m1> ^value MEDIUM ^opposite FAR)
	(<m2> ^value MEDIUM ^opposite NEAR)
	(<f> ^value FAR ^opposite NEAR)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*size
   (state <ss> ^type state)
-->
	(<ss> ^size <s> <m1> <m2> <l> <none>)
	(<s> ^value SMALL ^opposite LARGE)
	(<m1> ^value MEDIUM ^opposite LARGE)
	(<m2> ^value MEDIUM ^opposite SMALL)
	(<l> ^value LARGE ^opposite SMALL)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*duration
   (state <ss> ^type state)
-->
	(<ss> ^duration <as> <s> <m1> <m2> <f> <ef> <none>)
	(<as> ^value VERY_SHORT ^opposite VERY_LONG)
	(<s> ^value SHORT ^opposite LONG)
	(<m1> ^value MEDIUM ^opposite VERY_LONG)
	(<m2> ^value MEDIUM ^opposite VERY_SHORT)
	(<f> ^value LONG ^opposite SHORT)
	(<ef> ^value VERY_LONG ^opposite VERY_SHORT)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*tempo
   (state <ss> ^type state)
-->
	(<ss> ^tempo <as> <s> <m1> <m2> <f> <ef> <none>)
	(<as> ^value ALMOST_STILL ^opposite EXTREMELY_FAST)
	(<s> ^value SLOW ^opposite FAST)
	(<m1> ^value MEDIUM ^opposite EXTREMELY_FAST)
	(<m2> ^value MEDIUM ^opposite ALMOST_STILL)
	(<f> ^value FAST ^opposite SLOW)
	(<ef> ^value EXTREMELY_FAST ^opposite ALMOST_STILL)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*smoothness
   (state <ss> ^type state)
-->
	(<ss> ^smoothness <sm> <r1> <r2> <st> <none>)
	(<sm> ^value SMOOTH ^opposite STACCATO)
	(<r1> ^value REGULAR ^opposite STACCATO)
	(<r2> ^value REGULAR ^opposite SMOOTH)
	(<st> ^value STACCATO ^opposite SMOOTH)
	(<none> ^value NONE  ^opposite NONE)
}

sp { initialize*state*energy
   (state <ss> ^type state)
-->
	(<ss> ^energy <an> <l> <m1> <m2> <h> <eh> <none>)
	(<an> ^value ALMOST_NONE ^opposite EXTREMELY_HIGH)
	(<l> ^value LOW ^opposite HIGH)
	(<m1> ^value MEDIUM ^opposite EXTREMELY_HIGH)
	(<m2> ^value MEDIUM ^opposite ALMOST_NONE)
	(<h> ^value HIGH ^opposite LOW)
	(<eh> ^value EXTREMELY_HIGH ^opposite ALMOST_NONE)
	(<none> ^value NONE  ^opposite NONE)
}