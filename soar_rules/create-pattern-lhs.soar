# Create the Left Hand Side of a Pattern

sp { propose*create*pattern*lhs
	(state <s> ^name viewpoints-agent
			   ^phase create-pattern-lhs
			   ^is-pattern-lhs-set FALSE
			   ^is-pattern-rhs-set FALSE)
	-->
	(<s> ^operator <o> +)
	(<o> ^name create-pattern-lhs)
}

sp { propose*change*phase*update*internal*variables
	(state <s> ^name viewpoints-agent
			   ^phase create-pattern-lhs
			   ^is-pattern-lhs-set TRUE
			   ^is-pattern-rhs-set FALSE)
	-->
	(<s> ^operator <o> +)
	(<o> ^name change-phase-update-internal-variables)
}

sp { apply*change*phase*update*internal*variables
	(state <s> ^name viewpoints-agent
			   ^operator.name change-phase-update-internal-variables
			   ^phase <phase>
			   ^io.output-link <olink>)
	-->
	(<s> ^phase <phase> -)
	(<s> ^phase update-internal-variables)
	(<olink> ^outputs.done <done>)
}

sp { propose*set*pattern*lhs*exists
	(state <s> ^name create-pattern-lhs
			   ^superstate <ss>)
	(<ss> ^is-pattern-lhs-set FALSE
		  ^is-pattern-rhs-set FALSE
		  ^history.node <hnode>
		  ^patterns.node.input.gesture.uuid <pat-g-uuid>
		  ^history-index <hindex>)
	(<hnode> ^index <hindex>
			 ^output.uuid <pat-g-uuid>)
	-->
	(<s> ^operator <o> +)
	(<o> ^name set-pattern-lhs-exists)
	(write (crlf) | REACHED PROPOSE SET PATTERN LHS EXISTS |)
}

sp { propose*set*pattern*lhs*new
	(state <s> ^name create-pattern-lhs
			   ^superstate <ss>)
	(<ss> ^is-pattern-lhs-set FALSE
		  ^is-pattern-rhs-set FALSE
		  ^history.node <hnode>
		  ^history-index <hindex>)
	(<hnode> ^index <hindex>
			 ^output.uuid <lhs-uuid>)
	(<ss> -^patterns.node.input.gesture.uuid <lhs-uuid>)
	-->
	(<s> ^operator <o> +)
	(<o> ^name set-pattern-lhs-new)
	(write (crlf) | REACHED PROPOSE SET PATTERN LHS NEW |)
}

sp { apply*set*pattern*lhs*exists
	(state <s> ^name create-pattern-lhs
			   ^superstate <ss>
			   ^operator.name set-pattern-lhs-exists)
	(<ss> ^is-pattern-lhs-set <is-pattern-lhs-set>)
	-->
	(<ss> ^is-pattern-lhs-set <is-pattern-lhs-set> -)
	(<ss> ^is-pattern-lhs-set TRUE)
	(write (crlf) | REACHED APPLY SET PATTERN LHS EXISTS |)
}

sp { apply*set*pattern*lhs*new
	(state <s> ^name create-pattern-lhs
			   ^superstate <ss>
			   ^operator.name set-pattern-lhs-new)
	(<ss> ^history.node <hnode>
		  ^patterns <pat>
		  ^history-index <hindex>
		  ^is-pattern-lhs-set <is-pattern-lhs-set>)
	(<hnode> ^index <hindex>
			 ^output <lhs>)
	-->
	(<pat> ^node <pat-node>)
	(<pat-node> ^input.gesture (deep-copy <lhs>)
				^outputs <outs>
				^best-output.gesture <bo-g>)
	(<ss> ^is-pattern-lhs-set <is-pattern-lhs-set> -)
	(<ss> ^is-pattern-lhs-set TRUE)
	(write (crlf) | REACHED APPLY SET PATTERN LHS NEW |)
}