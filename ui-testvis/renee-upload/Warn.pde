final static int EAST = 2;
final static int WEST = 8;
int result;
float x,y,z;

void setup() {
	size(400,400);
	frameRate(30);  
	result = 0;
	x = width/2;
	y = height/2;
}

void draw() {
	background(0);
	switch(result) {
		case EAST: x++; z++; break;
		case WEST: x--; z--; break;
	}
	fill(255);
	rect(x,y,10,10);

	stroke(255);
	arc(400, 200, z, 400, PI / 2, 3 * PI / 2); // 180 degrees
}

void keyPressed(){
	switch(key) {
		case('d'):case('D'):result |=EAST;break;
		case('a'):case('A'):result |=WEST;break;
	}
}

void keyReleased(){  
	switch(key) {
		case('d'):case('D'):result ^=EAST;break;
		case('a'):case('A'):result ^=WEST;break;
	}
}
