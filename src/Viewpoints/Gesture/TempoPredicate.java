package Viewpoints.Gesture;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PVector;
import Viewpoints.Gesture.Predicates.TEMPO;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;

public class TempoPredicate extends ViewpointPredicate {
	public TempoPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}

	public TempoPredicate(float predNumber) {
		super(predNumber);
	}


	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public TEMPO classifyPredicate(float predNumber) {
		if (predNumber < 30.0f*ViewpointPredicate.ASSUMED_FRAMERATE) {
			return TEMPO.ALMOST_STILL;
		} else if (predNumber < 50.0f*ViewpointPredicate.ASSUMED_FRAMERATE) {
			return TEMPO.SLOW;
		} else if (predNumber < 80.0f*ViewpointPredicate.ASSUMED_FRAMERATE) {
			return TEMPO.MEDIUM;
		} else if (predNumber < 120.0f*ViewpointPredicate.ASSUMED_FRAMERATE) {
			return TEMPO.FAST;
		} else if (predNumber >= 160.0f*ViewpointPredicate.ASSUMED_FRAMERATE){
			return TEMPO.EXTREMELY_FAST;
		}
		return TEMPO.NONE; // something went wrong
	}
	
	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		if (skelPositions.size() < 2) {
			throw new Error("calcPredNumber: too few (< 2) skeletons !");
		}
		Body inCurSkel = skelPositions.get(skelPositions.size()-1);
		Body inPrevSkel = skelPositions.get(skelPositions.size()-2);
		
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff(	inCurSkel, inCurSkel.getTimestamp(), 
														inPrevSkel, inPrevSkel.getTimestamp());
		
		HashMap<JIDX, Float> tempos = new HashMap<JIDX, Float>();

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				tempos.put(ji, diffSkel.get(ji).mag());
			}
		}
		
		float tempoVal = getMax(tempos) * valueMultiplier;
		//System.out.println("Tempo Val: "+tempoVal);
		return tempoVal;
	}
}
