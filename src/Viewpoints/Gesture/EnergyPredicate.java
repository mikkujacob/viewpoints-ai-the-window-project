package Viewpoints.Gesture;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PVector;
import Viewpoints.Gesture.Predicates.ENERGY;
import Viewpoints.Gesture.Predicates.TEMPO;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;

public class EnergyPredicate extends ViewpointPredicate {
	public EnergyPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}

	public EnergyPredicate(float predNumber) {
		super(predNumber);
	}
	
	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public ENERGY classifyPredicate(float predNumber) {
		if (predNumber < 1500) {
			return ENERGY.ALMOST_NONE;
		} else if (predNumber < 3500) {
			return ENERGY.LOW;
		} else if (predNumber < 6500) {
			return ENERGY.MEDIUM;
		} else if (predNumber < 10000) {
			return ENERGY.HIGH;
		} else {
			return ENERGY.EXTREMELY_HIGH;
		}
	}
	
	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		if (skelPositions.size() < 2) {
			throw new Error("calcPredNumber: too few (< 2) skeletons !");
		}
		Body inCurSkel = skelPositions.get(skelPositions.size()-1);
		Body inPrevSkel = skelPositions.get(skelPositions.size()-2);
		
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff(inCurSkel, inCurSkel.getTimestamp(),
													  inPrevSkel, inPrevSkel.getTimestamp());

		float energy = 0f;

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				energy += diffSkel.get(ji).mag();
			}
		}
		return energy * valueMultiplier;
	}
}
