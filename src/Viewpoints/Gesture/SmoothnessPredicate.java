package Viewpoints.Gesture;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PVector;
import Viewpoints.Gesture.Predicates.SMOOTHNESS;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;

public class SmoothnessPredicate extends ViewpointPredicate {
	public SmoothnessPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}
	
	public SmoothnessPredicate(float predNumber) {
		super(predNumber);
	}


	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public SMOOTHNESS classifyPredicate(float predNumber) {
		if (predNumber < 100 * ASSUMED_FRAMERATE) {
			return SMOOTHNESS.SMOOTH;
		} else if (predNumber < 200 * ASSUMED_FRAMERATE) {
			return SMOOTHNESS.REGULAR;
		} else {
			return SMOOTHNESS.STACCATO;
		}
	}

	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		if (skelPositions.size() < 3) {
			throw new Error("calcPredNumber: too few (< 3) skeletons !"); 
		}
		
		Body inCurSkel = skelPositions.get(skelPositions.size()-1);
		Body inPrevSkel = skelPositions.get(skelPositions.size()-2);
		Body inPrev2Skel = skelPositions.get(skelPositions.size()-3);


		// get velocity between each pair of skeleton positions
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff  (inCurSkel, inCurSkel.getTimestamp(),
														inPrevSkel, inPrevSkel.getTimestamp());
		HashMap<JIDX, PVector> diff2Skel = getMvmtDiff (inPrevSkel, inPrevSkel.getTimestamp(),
														inPrev2Skel, inPrev2Skel.getTimestamp());

		// get acceleration by taking difference of velocities
		HashMap<JIDX, PVector> accSkel = getMvmtDiff(diffSkel, (inCurSkel.getTimestamp() + inPrevSkel.getTimestamp())/2.0f,
													 diff2Skel, (inPrevSkel.getTimestamp() + inPrev2Skel.getTimestamp())/2.0f);

		HashMap<JIDX,Float> smoothnesses = new HashMap<JIDX,Float>();
		for (JIDX ji : accSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				smoothnesses.put(ji, Math.abs(accSkel.get(ji).mag()));
			}
		}
		float smoothness = getMax(smoothnesses) * valueMultiplier; // smoothness is maximum acceleration
		return smoothness;
	}
}
