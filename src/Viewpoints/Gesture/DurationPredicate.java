package Viewpoints.Gesture;

import java.util.ArrayList;
import Viewpoints.Gesture.Predicates.DURATION;
import Viewpoints.Gesture.Predicates.TEMPO;
import Viewpoints.Shared.Body;

public class DurationPredicate extends ViewpointPredicate {
	public DurationPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}
	
	public DurationPredicate(float predNumber) {
		super(predNumber);
	}


	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public DURATION classifyPredicate(float predNumber) {
		if (predNumber < 10.0f / ASSUMED_FRAMERATE) {
			return DURATION.VERY_SHORT;
		} else if (predNumber < 20.0f / ASSUMED_FRAMERATE) {
			return DURATION.SHORT;
		} else if (predNumber < 40.0f / ASSUMED_FRAMERATE) {
			return DURATION.MEDIUM;
		} else if (predNumber < 60.0f / ASSUMED_FRAMERATE) {
			return DURATION.LONG;
		} else if (predNumber >= 60.0f / ASSUMED_FRAMERATE){
			return DURATION.VERY_LONG;
		}
		return DURATION.NONE;
	}

	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		return skelPositions.get(skelPositions.size() - 1).getTimestamp() - skelPositions.get(0).getTimestamp();
	}
}
