package Viewpoints.FileUtilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import Viewpoints.Gesture.Gesture;
import Viewpoints.Shared.JointSpaceGesture;

/**
 * Utility class meant for methods related to writing gestures to files
 * @author mjacob6
 *
 */
public class FileUtilities {
	
	/**
	 * 
	 * @param path
	 * @param outputGesture
	 */
	public static void writeToText(String path, Gesture outputGesture){
		
		FileWriter fstream = null;
		BufferedWriter out = null;
		String currentDirectory = "";
		
		try {
			currentDirectory = new java.io.File( "." ).getCanonicalPath();
			
			if(currentDirectory.indexOf(File.separator + "bin") != -1)
			{
				currentDirectory += File.separator + "..";
			}
			
			currentDirectory += File.separator + "file_communications";
			
			fstream = new FileWriter(currentDirectory + File.separator + path, false);
			out = new BufferedWriter(fstream);
			
//			System.out.println("Writing: " + outputGesture.toString());
			
			out.write(outputGesture.toString());
			
			System.out.println("Done Writing " + path);
		} catch (Exception e) {
			System.err.println("failed to output Gesture: " + e.getMessage());
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	public static Gesture readFromText(String path){
		
		FileReader fstream = null;
		BufferedReader in = null;
		String inputGesture = "";
		String currentDirectory = "";
		try {
			currentDirectory = new java.io.File( "." ).getCanonicalPath();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}
		
		try {
			if(currentDirectory.indexOf(File.separator + "bin") != -1)
			{
				currentDirectory += File.separator + "..";
			}
			
			currentDirectory += File.separator + "file_communications";
			
			fstream = new FileReader(currentDirectory + File.separator + path);
			in = new BufferedReader(fstream);
			String line = "";
			
			while((line = in.readLine()) != null)
			{
				inputGesture += line + "\n";
			}
			
			//System.out.println("Done Reading: " + inputGesture);
		} catch (Exception e) {
			System.err.println("failed to output Gesture: " + e.getMessage());
		}
		finally {
			try {
				if (null != in)
					in.close();
				
				} catch (IOException e) {
				//e.printStackTrace();
			}
		}
		
		//System.out.println(Gesture.fromString(inputGesture));
		return Gesture.fromString(inputGesture);
	}
	
	/**
	 * 
	 * @param path
	 */
	public static void clearText(String path){
		
		FileWriter fstream = null;
		BufferedWriter out = null;
		String currentDirectory = "";
		
		try {
			currentDirectory = new java.io.File( "." ).getCanonicalPath();
			
			if(currentDirectory.indexOf(File.separator + "bin") != -1)
			{
				currentDirectory += File.separator + "..";
			}
			
			currentDirectory += File.separator + "file_communications";
			
			fstream = new FileWriter(currentDirectory + File.separator + path, false);
			out = new BufferedWriter(fstream);
			
//			System.out.println("Clearing");
			
			out.write("");
			
			System.out.println("Done Clearing " + path);
		} catch (Exception e) {
			System.err.println("failed to clear text: " + e.getMessage());
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param path
	 * @param outputGestureJoints
	 */
	public static Boolean serializeJointsGesture(String path, JointSpaceGesture outputGestureJoints)
	{
		String currentDirectory = "";
		
		try {
			currentDirectory = new java.io.File( "." ).getCanonicalPath();
		
			if(currentDirectory.indexOf(File.separator + "bin") != -1)
			{
				currentDirectory += File.separator + "..";
			}
			
			currentDirectory += File.separator + "file_communications";
			
			FileOutputStream fileoutstream = new FileOutputStream(currentDirectory + File.separator + path);
			ObjectOutputStream objectoutstream = new ObjectOutputStream(fileoutstream);
			objectoutstream.writeObject(outputGestureJoints);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		}
		catch (FileNotFoundException e)
		{
			//e.printStackTrace();
			
			return false;
		}
		catch (EOFException e)
		{
			//e.printStackTrace();
			
			return false;
		}
		catch (IOException e)
		{
			//e.printStackTrace();
			
			return false;
		}
		
		System.out.println("Done Writing " + path);
		
		return true;
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	public static JointSpaceGesture deserializeJointsGesture(String path)
	{
		String currentDirectory = "";
		JointSpaceGesture readGesture = new JointSpaceGesture();
		
		try 
		{
			currentDirectory = new java.io.File( "." ).getCanonicalPath();
		} 
		catch (IOException e) 
		{
			//e.printStackTrace();
			
			return null;
		}
		
		if(currentDirectory.indexOf(File.separator + "bin") != -1)
		{
			currentDirectory += File.separator + "..";
		}
		
		currentDirectory += File.separator + "file_communications";
		
		try
		{
			File infile = new File(currentDirectory + File.separator + path);
			FileInputStream fileinstream = new FileInputStream(infile);
			
			if(fileinstream.available() == 0)
			{
				return null;
			}
			
			ObjectInputStream objectinstream = new ObjectInputStream(fileinstream);
			readGesture = (JointSpaceGesture) objectinstream.readObject();
			objectinstream.close();
		}
		catch (FileNotFoundException e)
		{
			//e.printStackTrace();
			
			return null;
		}
		catch (EOFException e)
		{
			//e.printStackTrace();
			
			return null;
		}
		catch (IOException e)
		{
			//e.printStackTrace();
			
			return null;
		} 
		catch (ClassNotFoundException e) 
		{
			//e.printStackTrace();
			
			return null;
		}
		
//		System.out.println("Done Reading");
		
		return readGesture;
	}
	
	/**
	 * 
	 * @param path
	 */
	public static void writeToFile(String path, String text){
		
		FileWriter fstream = null;
		BufferedWriter out = null;
		
		try {
			fstream = new FileWriter(path, false);
			out = new BufferedWriter(fstream);
			
			out.write(text);
			
//			System.out.println("Done Writing " + path);
		} catch (Exception e) {
			System.err.println("failed to write text: " + e.getMessage());
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				//e.printStackTrace();
			}
		}
	}
}
