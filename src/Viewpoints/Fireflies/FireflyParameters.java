package Viewpoints.Fireflies;

import processing.core.*;

public class FireflyParameters {
	public float LAG_FACTOR = 0.9f;
	public float ANXIETY = 5.0f;
	public float FLIGHT_SPEED = 0.0f;
	public float BASIC_SPEED = 0.0f;
	public float TURN_RADIUS = 0.0f;
	public float BASIC_RADIUS = 0.0f;
	public float ANGSPEED_FACTOR = 0.0f;
	public float PITCH_FACTOR = 0.0f;
	public float PITCH_CUTOFF = 0.0f;
	public float DEVIATION_FACTOR = 0.0f;
	public float DEVIATION_CUTOFF = 0.0f;
	public float INTENCITY_FLUCTUATION_CUTOFF = 0.05f;
	public float ELECTRIZATION = 0.4f;
	public float SPARKLENESS = 0.7f;
	public float DEFAULT_FADE = 64.0f;
	public float FADE = DEFAULT_FADE;
	public float SPECTRE_PITCH = 0.5f;
	
	public FireflyParameters() {
		
	}
	
	public FireflyParameters(float flightSpeed, float turnRadius) {
		adjustToScale(flightSpeed, turnRadius);
	}
	
	public void adjustToScale(float flightSpeed, float turnRadius) {
		FLIGHT_SPEED = flightSpeed;
		BASIC_SPEED = flightSpeed;
		TURN_RADIUS = turnRadius;
		BASIC_RADIUS = turnRadius;
		ANGSPEED_FACTOR = FLIGHT_SPEED/TURN_RADIUS;
		PITCH_FACTOR = ANGSPEED_FACTOR * 5.0f;
		PITCH_CUTOFF = 3 * ANGSPEED_FACTOR;
		DEVIATION_FACTOR = ANGSPEED_FACTOR * 1.0f;
		DEVIATION_CUTOFF = ANGSPEED_FACTOR / 2;
	}
	
	public void setFlightSpeed(float flightSpd) {
		ANXIETY *= FLIGHT_SPEED/flightSpd;
		FLIGHT_SPEED = flightSpd;
		ANGSPEED_FACTOR = FLIGHT_SPEED/TURN_RADIUS;
	}
	
	public void setTurnRadius(float turnR) {
		ANXIETY *= TURN_RADIUS/turnR;
		TURN_RADIUS = turnR;
		ANGSPEED_FACTOR = FLIGHT_SPEED/TURN_RADIUS;
	}
	
	public void setAnxiety(float anxiety) {
		ANXIETY = anxiety;
	}
	
	public void setElectrization(float electrization) {
		ELECTRIZATION = electrization;
		
		if (ELECTRIZATION < 0.0f) {
			ELECTRIZATION = 0.0f;
		} else if (ELECTRIZATION > 1.0f) {
			ELECTRIZATION = 1.0f;
		}
	}
	
	public void setSparkleness(float sparkleness) {
		SPARKLENESS = sparkleness;
		
		if (SPARKLENESS < 0.0f) {
			SPARKLENESS = 0.0f;
		} else if (SPARKLENESS > 1.0f) {
			SPARKLENESS = 1.0f;
		}
	}
	
	public void setFade(float exposure) {
		FADE = exposure;
		
		if (FADE < 0.0f) {
			FADE = 0.0f;
		} else if (FADE > 255.0f) {
			FADE = 255.0f;
		}
	}
	
	public void setSpectrePitch(float spectrePitch) {
		SPECTRE_PITCH = spectrePitch;
		
		if (SPECTRE_PITCH < 0.0f) {
			SPECTRE_PITCH = 0.0f;
		} else if (SPECTRE_PITCH > 1.0f) {
			SPECTRE_PITCH = 1.0f;
		}
	}
}
