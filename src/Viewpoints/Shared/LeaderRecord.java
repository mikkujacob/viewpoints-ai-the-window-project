package Viewpoints.Shared;

import java.util.ArrayList;
import java.util.TreeMap;

// records body positions at certain times and generate body positions
// at intermediate times by interpolation
public class LeaderRecord implements java.io.Serializable {

	// serial ID of LeaderRecord
	private static final long serialVersionUID = 1L;

	// tree map storing body positions and the time this body has been recorded
	private TreeMap<Float, Body> bodies = new TreeMap<Float, Body>();

	// relative acceleration of the replay against the original scene
	private float replaySpeed = 1;

	// whether the replay has to be repeated or not
	// if yes, it is repeated backward once finished until it is back to the beginning
	private boolean isRepetitionOn = true;

	// step by which the speed is increased or decreased
	private float replaySpeedStep = 0.1f;

	// minimal replay speed
	private float replaySpeedMin = 0;

	// whether the replay did not begin yet or not
	private boolean firstReplay = true;

	// total time elapsed since the beginning of the recording or the replay
	private float totalTime = 0.0f;
	
	private float percentPassed = 0.0f;

	// constructs a LeaderRecord with default values
	public LeaderRecord() {
	}
	
	public LeaderRecord(ArrayList<Body> iBodies) {
		if (0 != iBodies.size()) {
			float startingTime = iBodies.get(0).getTimestamp();
			
			for (Body body : iBodies) {
				bodies.put(body.getTimestamp() - startingTime, body);
			}
		}
	}
	
	public boolean isFresh() {
		return firstReplay;
	}
	
	public void setReplaySpeed(float replaySpeed) {
		this.replaySpeed = replaySpeed;
	}
	
	// updates whether the replay has to be repeated or not
	public void setIsRepetitionOn(boolean isRepetitionOn) {
		
		this.isRepetitionOn = isRepetitionOn;
		
	}
	
	public boolean isRepetitionOn() {
		return this.isRepetitionOn;
	}

	// sets the speed back to its original value
	public void reinitializeSpeed() {

		replaySpeed = 1;

	}

	// decreases the replay speed by its step, unless it reached its minimal value
	public void decreaseSpeed () {

		// if the replay speed has not reached its minimal value
		if (replaySpeed > replaySpeedMin) {

			// decreases the replay speed by its step
			replaySpeed -= replaySpeedStep;

			// if the replay speed is less than its minimal value
			if (replaySpeed < replaySpeedMin) {

				// sets the replay speed to its minimal value
				replaySpeed = replaySpeedMin;

			}

		}
	}

	// increases the replay speed by its step
	public void increaseSpeed () {

		replaySpeed += replaySpeedStep;

	}

	// records the position of the body at this time
	public void recordFrame(float time, Body body) {

		// if no body has been recorded yet
		if (bodies.isEmpty()) {

			// the time of the first body of the recording is 0 to calibrate the beginning
			time = 0;
			
			// the total time elapsed since the beginning of the recording is initialized
			totalTime = 0;

		}

		// the total time is increased by the time elapsed since the last body has been recorded
		totalTime += time;

		// adds this body and its time to the sequence of bodies
		bodies.put(totalTime, body);

	}

	// returns the position of the body at this time (real or interpolated)
	public Body replayFrame(float time) {

		// position of the body to be returned
		Body body = new Body();
		
		float replay_time = 0;

		// if the replay did not begin yet
		if (firstReplay) {

			// the body to return is the first one of the sequence
			body = bodies.firstEntry().getValue();
			
			// the total time elapsed since the beginning of the replay is initialized to 0
			totalTime = 0;
			
			// the replay now began
			firstReplay = false;

			// the replay will be repeated
			if (isRepetitionOn) {

				// removes the last body of the sequence since it is meant to indicate the end of the sequence
				bodies.remove(bodies.lastKey());

			}

		}

		// otherwise, if the replay already began
		else {

			// the total time is increased by the time elapsed since
			// the last body replayed times the replay speed
			totalTime += time*replaySpeed;

			// time corresponding to the position of the body to retrieve according to the speed
			replay_time = totalTime;

			// number of times bodies has been browsed (in a sense or the other)
			int nbCycles = (int) (replay_time/bodies.lastKey());

			// if the total time elapsed since the beginning of the replay
			// is greater than the total time of the recording
			if (replay_time >= bodies.lastKey()) {

				// if the replay will be repeated
				if (isRepetitionOn) {

					// if an even number of sequences has been played (in any sense)
					if (nbCycles%2 == 0) {

						// this sequence is played forward
						replay_time %= bodies.lastKey();

					}

					// otherwise, if an odd number of sequences has been played (in any sense)
					else {

						// this sequence is played backward
						replay_time = (bodies.lastKey() - (replay_time - nbCycles*bodies.lastKey()));

					}

				}

				// otherwise, if the replay does not repeat
				else {

					replay_time = bodies.lastKey();
					// the body to return is the last one of the sequence
					body = bodies.lastEntry().getValue();

				}

			}

			// if the replay has to be repeated or the end of the sequence has not been reached yet
			if (isRepetitionOn||(replay_time < bodies.lastKey())) {

				// first time greater than the given one for which a frame has been recorded
				float time_greater = bodies.tailMap(replay_time, true).firstKey();

				// if a frame has been recorded for the given time
				if (replay_time == time_greater) {

					// the corresponding body position will be returned
					body = bodies.get(time_greater);

				}

				// no frame has been recorded for the given time
				else {

					// first time strictly less than the given one for which a frame has been recorded
					float time_less = bodies.lowerKey(replay_time);

					// time between the inferior and superior times in long to scale the coefficients to percentages
					float time_scale_long = time_greater - time_less;
					
					/*if (null == bodies.get(time_greater)) {
						int k = 0;
					}*/

					// the position of the body has to be interpolated from the positions at time_less and time_greater
					body = Body.interpolate(bodies.get(time_less), (time_greater - replay_time)/time_scale_long,
							bodies.get(time_greater), (replay_time - time_less)/time_scale_long);

				}

			}
		}

		percentPassed = replay_time / bodies.lastKey();
		// the position of the body corresponding to the given time is returned
		return body;

	}
	
	public float percentPassed() {
		return percentPassed;
	}
	
	public float getBasicDuration() {
		return bodies.lastKey();
	}

	public float getTimePlayed() {
		return totalTime;
	}
}
