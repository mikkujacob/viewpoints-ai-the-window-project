package Viewpoints.Visualisation.viz;

import java.awt.Point;

import processing.core.PGraphics;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.Visualisation.VAIGraphics;

/**
 * Sliding spotlight (pending)
 * 
 * @author toriscope
 * 
 */
public class TurnVisualization {

	final private MidpointChain mpChain = new MidpointChain(new Point.Float(100, 1000), 4);
	
	public void draw(final PGraphics buffer, final SimpleOpenNI kinectInput, final float[] avgUser) {

		boolean tracking = false;
		for (int i = 0; i < 10; i++) {
			if (kinectInput.isTrackingSkeleton(i)) {
				tracking = true;
				break;
			}
		}

		boolean vaiTurn = VAIGraphics.isVaiAnimating();

		final String turnString = vaiTurn ? "VAI's Turn" : "Your Turn";
		float x = avgUser[0] == avgUser[0] ? avgUser[0] : mpChain.getA().x;
		final PVector vaiPos = VAIGraphics.getVaiPos();
		if (vaiPos != null && vaiTurn)
			x = (vaiPos.x/2) + buffer.width / 2;
		float y = buffer.height - 10 + (tracking ? 0 : 100);

		mpChain.setA(new Point.Float(x, y));
		mpChain.smoothTowardA();
		final Point.Float b = mpChain.getB();

		buffer.fill(0, 0, 0, 200);
		buffer.rectMode(buffer.CORNER);
		buffer.rect(b.x - 5 - turnString.length() * 25 * .5f, b.y + 5, turnString.length() * 25, -50, 10);
		
		buffer.fill(255, 255, 255);
		buffer.textMode(buffer.CORNER);
		buffer.textSize(40);
		buffer.text(turnString, b.x - turnString.length() * 25 * .5f, b.y);
	}
}
