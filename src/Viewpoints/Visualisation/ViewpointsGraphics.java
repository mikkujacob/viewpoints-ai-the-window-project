package Viewpoints.Visualisation;

import java.util.Map;

import processing.core.PApplet;
import processing.core.PVector;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.JIDX;
import Viewpoints.Shared.LIDX;
import Viewpoints.Shared.Pair;

public class ViewpointsGraphics extends PApplet {
	public PVector getScreenPos(PVector pos) {
		PVector screenPos = PVector.mult(pos, 0.3f); //new PVector();
		screenPos.y = - screenPos.y;
		screenPos.z = 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	public void drawBody(Body body) 
	{
		strokeWeight(1);
		
		for (Map.Entry<LIDX, Pair<JIDX, JIDX>> limbEnds : LIDX.LIMB_ENDS().entrySet()) {
			PVector pos1 = getScreenPos(body.get(limbEnds.getValue().first));
			PVector pos2 = getScreenPos(body.get(limbEnds.getValue().second));
			line(pos1.x, pos1.y, pos2.x, pos2.y);
		}
	}
	
	public void drawVector(PVector origin, PVector vec) {
		strokeWeight(1);
		PVector pos1 = getScreenPos(origin);
		PVector pos2 = getScreenPos(PVector.add(origin, vec));
		line(pos1.x, pos1.y, pos2.x, pos2.y);
	}
}
