package Viewpoints.Visualisation;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.FileUtilities.FileUtilities;
import Viewpoints.Shared.Body;
import Viewpoints.Visualisation.viz.AlertVisualization;
import Viewpoints.Visualisation.viz.AlertVisualization.AnimState;
import Viewpoints.Visualisation.viz.AlertVisualization.Side;
import Viewpoints.Visualisation.viz.MidpointChain;
import Viewpoints.Visualisation.viz.TurnVisualization;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

public class ShadowGraphics extends BufferedGraphics {
	SimpleOpenNI kinectInput = null;
	private int BLACK = 0;
	private static final int OUTLINE_WIDTH = 10;
	
	private final AlertVisualization alertVis;
	private final TurnVisualization turnVis;

	public String perceptionActionStatusFileName = "PerceptionActionUserStatus.txt";
	
	public ShadowGraphics(SimpleOpenNI kinectInput, PApplet master) {
		preSetup(perceptionActionStatusFileName);
		
		alertVis = new AlertVisualization();
		turnVis = new TurnVisualization();
		this.kinectInput = kinectInput;
		PGraphics usrbuffer = master.createGraphics(kinectInput.depthWidth(), kinectInput.depthHeight());
		BLACK = usrbuffer.color(0);
		// make wide stroke for thick outline
		usrbuffer.strokeWeight(OUTLINE_WIDTH);
		// making points semi-transparent will create the blurring effect when a line of them is drawn
		usrbuffer.stroke(255, 255, 255, 255.0f /OUTLINE_WIDTH);
		super.init(usrbuffer);
	}

	
	@Override
	public void run() {
		while (true) {
			generateShadows();
			supply();
			writeHeartBeat();
		}
	}
	
	private void generateShadows() {
		buffer.beginDraw();
		buffer.background(0);
		PImage userImage = null;
		synchronized (kinectInput) {
			kinectInput.update();
			userImage = kinectInput.userImage();
		}
		userImage.loadPixels();
		int outlineCount = 0;
		
		// make wide stroke for thick outline
		buffer.strokeWeight(OUTLINE_WIDTH);
		// making points semi-transparent will create the blurring effect when a line of them is drawn
		buffer.stroke(255, 255, 255, 255.0f /OUTLINE_WIDTH);
	  
		// flatK is position in flat array of pixels. prevcolor is color on the left, upcolor is color on the top, currcolor is current color
		float[] avg = new float[]{0f, 0f};
		for (int j = 1, flatK = buffer.width; j < buffer.height; j++) {
			int prevcolor = usercolor(userImage.pixels[flatK]);
			flatK++;
			for (int i = 1; i < buffer.width; i++, flatK++) {
				int currcolor = usercolor(userImage.pixels[flatK]);
				int upcolor   = usercolor(userImage.pixels[flatK - buffer.width]);
				if (currcolor != prevcolor || currcolor != upcolor) {
					buffer.point(i, j);
					avg[0] += i;
					avg[1] += j;
					outlineCount++;
				}
				prevcolor = currcolor;
			}
		}

		// Calc avg.
		avg[0] /= outlineCount;
		avg[1] /= outlineCount;

		//System.out.println(outlineCount);
		alertVis.draw(buffer, kinectInput, userImage);
		turnVis.draw(buffer, kinectInput, avg);

		buffer.endDraw();
	}
	
	// returns black for no user and user color if there is some user
	private int usercolor(int userimgcolor) {
		float r = buffer.red(userimgcolor);
		float g = buffer.green(userimgcolor);
		float b = buffer.blue(userimgcolor);
		if (r == g && g == b) {
			return BLACK;
		} else {
			return userimgcolor;
		}
	}
}
