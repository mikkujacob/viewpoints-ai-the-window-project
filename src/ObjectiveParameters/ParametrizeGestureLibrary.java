package ObjectiveParameters;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Locale;

import Viewpoints.Shared.JointSpaceGesture;

public class ParametrizeGestureLibrary {
	public static final String GESTURE_DIR = "training-gestures/sad-golden";
	public static final String PARAMETERS_DIR = "gesture_parameters";
	
	public static void main(String[] args) {
		try {
			File gestureDir = new File(GESTURE_DIR);
			File[] gestureFiles = gestureDir.listFiles();
			ObjectiveParameterMonitor objectiveParameterMonitor = new ObjectiveParameterMonitor();
			for (File gestureFile : gestureFiles) {
				FileInputStream gestureFileIn = new FileInputStream(gestureFile);
				//FileInputStream gestureFileIn = new FileInputStream("gestures/MonApr0822-48-14GMT-04-002013.jsg");
				ObjectInputStream gestureIn = new ObjectInputStream(gestureFileIn);
				JointSpaceGesture gesture = (JointSpaceGesture)gestureIn.readObject();
				gesture.autotiming();
				gestureFileIn.close();
				HashMap<ParameterKey, Float> parameters = objectiveParameterMonitor.getParameters(gesture.getGestureFramesList());
				String clearName = gestureFile.getName().replaceFirst("[.][^.]+$", "");
				IO.out(PARAMETERS_DIR + "/" + clearName + ".param", parameters);
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
};
