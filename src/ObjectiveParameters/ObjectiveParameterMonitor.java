package ObjectiveParameters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import processing.core.PVector;
import Viewpoints.Shared.AveragingCore;
import Viewpoints.Shared.Body;
import Viewpoints.Shared.BodyDerivative;
import Viewpoints.Shared.JIDX;
import Viewpoints.Shared.LIDX;
import Viewpoints.Shared.Measure;
import Viewpoints.Shared.Pair;

public class ObjectiveParameterMonitor {
	private SpeedSensor speedSensor = new SpeedSensor();
	private AccelerationSensor accelerationSensor = new AccelerationSensor();
	private BoxSensor boxSensor = new BoxSensor();
	private BendingSensor bendingSensor = new BendingSensor();
	private ParameterSensor[] sensors = {speedSensor, accelerationSensor, boxSensor, bendingSensor};
	private AveragingCore averagingCore = new AveragingCore(0.5f);
	
	public HashMap<ParameterKey, Float> getParameters(ArrayList<Body> frames) 
	{
		HashMap<ParameterKey, ArrayList<Measure>> measures = new HashMap<ParameterKey, ArrayList<Measure>>();
		frames = scaleToStandardSize(frames);
		ArrayList<BodyDerivative> firstDerivative = BodyDerivative.derivative(frames);
		ArrayList<BodyDerivative> secondDerivative = BodyDerivative.derivative(firstDerivative);
		for (ParameterSensor sensor : sensors) {
			measures.putAll(sensor.sense(frames, firstDerivative, secondDerivative));
		}
		return symmetrize(aggregate(derive(measures)));
	}
	
	private ArrayList<Body> scaleToStandardSize(ArrayList<Body> frames) {
		ArrayList<Body> standartizedFrames = new ArrayList<Body>();
		HashMap<LIDX, Pair<JIDX, JIDX>> lidxmap = LIDX.LIMB_ENDS();
		float sumsize = 0.0f;
		
		for (Body body : frames) {
			for (Map.Entry<LIDX, Pair<JIDX, JIDX>> lidxmapEntry : lidxmap.entrySet()) {
				Pair<JIDX, JIDX> jidxpair = lidxmapEntry.getValue();
				sumsize += PVector.sub(body.get(jidxpair.second), body.get(jidxpair.first)).mag();
			}
		}
		
		float factor = frames.size() * lidxmap.size() / sumsize;
		
		for (Body body: frames) {
			Body scaled = (Body)body.clone();
	
			for (JIDX jidx : JIDX.ALL_JIDX) {
				scaled.get(jidx).mult(factor);
			}
			
			standartizedFrames.add(scaled);
		}
				
		return standartizedFrames;
	}
	
	private HashMap<ParameterKey, ArrayList<Measure>> derive(HashMap<ParameterKey, ArrayList<Measure>> measures) {
		HashMap<ParameterKey, ArrayList<Measure>> derivedMeasures = new HashMap<ParameterKey, ArrayList<Measure>>();
		for (Map.Entry<ParameterKey, ArrayList<Measure>> keyedMeasures : measures.entrySet()) {
			ParameterKey basicKey = keyedMeasures.getKey();
			ArrayList<Measure> basicMeasures = keyedMeasures.getValue();
			derivedMeasures.put(basicKey, basicMeasures);
			if (ParameterType.isSigned(basicKey.getParamType())) {
				ParameterKey absKey = basicKey.deriveModifiedData(DataDerivationType.ABS);
				ArrayList<Measure> absMeasures = new ArrayList<Measure>();
				for (Measure basicMeasure : basicMeasures) {
					absMeasures.add(new Measure(basicMeasure.getTimestamp(), Math.abs(basicMeasure.getValue())));
				}
				derivedMeasures.put(absKey, absMeasures);
			}
		}
		return derivedMeasures;
	}
	
	private HashMap<ParameterKey, Float> aggregate(HashMap<ParameterKey, ArrayList<Measure>> measures) {
		HashMap<ParameterKey, Float> aggregations = new HashMap<ParameterKey, Float>();
		for (Map.Entry<ParameterKey, ArrayList<Measure>> keyedMeasures : measures.entrySet()) {
			ArrayList<Measure> theMeasures = keyedMeasures.getValue();
			ParameterKey basicKey = keyedMeasures.getKey();
			ParameterKey avgKey = basicKey.deriveAggregation(AggregationType.AVE);
			ParameterKey maxKey = basicKey.deriveAggregation(AggregationType.MAX);
			float ave = 0.0f;
			for (Measure measure : theMeasures) {
				ave += measure.getValue();
			}
			ave /= measures.size();
			ArrayList<Measure> smoothedMeasures = averagingCore.apply(keyedMeasures.getValue());
			float max = -Float.MAX_VALUE;
			for (Measure measure : smoothedMeasures) {
				if (max < measure.getValue()) {
					max = measure.getValue();
				}
			}
			aggregations.put(avgKey, ave);
			aggregations.put(maxKey, max);
			if (ParameterType.isSigned(basicKey.getParamType()) && DataDerivationType.ABS != basicKey.getDerivationType()) {
				ParameterKey minKey = basicKey.deriveAggregation(AggregationType.MIN);
				ParameterKey rangeKey = basicKey.deriveAggregation(AggregationType.RANGE);
				float min = Float.MAX_VALUE;
				for (Measure measure : smoothedMeasures) {
					if (min > measure.getValue()) {
						min = measure.getValue();
					}
				}
				aggregations.put(minKey, min);
				aggregations.put(rangeKey, max - min);
			}
			ParameterKey momentumKey = basicKey.deriveAggregation(AggregationType.MOMENTUM);
			ParameterKey reverseMomentumKey = basicKey.deriveAggregation(AggregationType.REVERSE_MOMENTUM);
			float momentum = 0.0f;
			float reverseMomentum = 0.0f;
			float startTime = theMeasures.get(0).getTimestamp();
			float duration = theMeasures.get(theMeasures.size() - 1).getTimestamp() - startTime;
			for (Measure measure : theMeasures) {
				float relativeTime = (measure.getTimestamp() - startTime) / duration;
				momentum += relativeTime * measure.getValue();
				reverseMomentum += (1.0 - relativeTime) * measure.getValue();
			}
			aggregations.put(momentumKey, momentum);
			aggregations.put(reverseMomentumKey, reverseMomentum);
		}
		return aggregations;
	}
	
	private HashMap<ParameterKey, Float> symmetrize(HashMap<ParameterKey, Float> parameters) {
		HashMap<ParameterKey, Float> symmetrization = new HashMap<ParameterKey, Float>();
		for (Map.Entry<ParameterKey, Float> parameter : parameters.entrySet()) {
			ParameterKey key = parameter.getKey();
			Float paramValue = parameter.getValue();
			if (!"LEFT".equals(key.getJidx().toString().substring(0, 4))) {
				if (!"RIGH".equals(key.getJidx().toString().substring(0, 4))) {
					symmetrization.put(key, paramValue);
				}
				continue;
			}
			ParameterKey symKey = key.symKey();
			Float symParamValue = parameters.get(symKey);
			ParameterKey midKey = key.deriveSymmetryAggregation(SymmetryAggregationType.MID);
			ParameterKey deltaKey = key.deriveSymmetryAggregation(SymmetryAggregationType.DELTA);
			ParameterKey leadKey = key.deriveSymmetryAggregation(SymmetryAggregationType.LEAD);
			ParameterKey trailKey = key.deriveSymmetryAggregation(SymmetryAggregationType.TRAIL);
			symmetrization.put(midKey, (paramValue + symParamValue)/2);
			symmetrization.put(deltaKey, Math.abs(symParamValue - paramValue));
			symmetrization.put(leadKey, Math.max(paramValue, symParamValue));
			symmetrization.put(trailKey, Math.min(paramValue, symParamValue));
		}
		return symmetrization;
	}
}
