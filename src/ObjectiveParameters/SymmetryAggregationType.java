package ObjectiveParameters;

public enum SymmetryAggregationType {
	NONE,
	MID,
	DELTA,
	LEAD,
	TRAIL
}
